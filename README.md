# PPRL entity mask service

This service provides entity masking capabilities for Bloom filter based PPRL.

## Installation

To run an ephemeral container, use the line below.

```
docker run --rm -p 8080:8080 -d quay.io/mds4ul/pprl-mask:latest
```

You can also compile this project by hand and run the standalone executable JAR.
Dependencies are stored in `target/libs` and are referenced in the JAR manifest file.

```
mvn clean package
cd target
java -jar pprl-mask-service-1.0-SNAPSHOT.jar
```

## Configuration

You can configure the service by manually overwriting the [app.properties](src/main/resources/app.properties) file.
It is also possible to define system properties when running the service on the command line.

**Option** | **System property** | **Description** | **Default**
:--|:--|:--|:--
`prng-algorithm` | `-Dservice.prng-algorithm` | PRNG algorithm to use for operations that require randomness | L32X64MixRandom

> L32X64MixRandom is the default algorithm with the introduction of the new Java RNG API.
> Unfortunately, it seems that [the Alpine build of Temurin only supports the legacy algorithms](https://github.com/adoptium/temurin-build/issues/2843).
> The Docker image is configured to use the legacy Random algorithm.
> An error on startup will be logged if the requested algorithm is not available.

## Example request

```python
import json
import requests

r = requests.post("http://localhost:8080", json={
    "masking": {
        "tokenSize": 2,
        "hashValues": 5,
        "hashFunction": "HMAC-SHA256",
        "key": "s3cr3t",
        "seed": 123,
        "hashStrategy": "randomHash",
        "filterType": "CLKRBF",
        "hardeners": [
            {
                "name": "balance"
            },
            {
                "name": "permute",
                "seed": 456
            },
            {
                "name": "xorFold"
            }
        ]
    },
    "entities": [
        {
            "id": "001",
            "firstName": "maximilian",
            "lastName": "jugl",
            "birthDate": "19980629"
        },
        {
            "id": "002",
            "firstName": "lars",
            "lastName": "hempel",
            "birthDate": "19960212"
        }   
    ],
    "attributes": [
        {
            "name": "firstName",
            "weight": 2,
            "averageTokenCount": 11
        },
        {
            "name": "lastName",
            "weight": 2,
            "averageTokenCount": 5
        },
        {
            "name": "birthDate",
            "weight": 1.5,
            "averageTokenCount": 9
        }
    ]
})

print(json.dumps(r.json(), indent=2))
# {
#   "entities": [
#     {
#       "id": "001",
#       "value": "UunAZyUznBpz0_3_FtYk4w4AqXRRBMOMTPbNDwg="
#     },
#     {
#       "id": "002",
#       "value": "JPSTdVKH3VKBPTUeuB9fjxzJZWs72CL9x9mZQw=="
#     }
#   ]
# }
```

## License

MIT.