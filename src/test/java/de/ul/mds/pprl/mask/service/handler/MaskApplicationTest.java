package de.ul.mds.pprl.mask.service.handler;

import de.ul.mds.pprl.mask.service.*;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MaskApplicationTest {

    @Test
    public void testThrowOnUnimplementedPRNGAlgorithm() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MaskApplication(
                new MaskApplicationProperties("foobar")
        ));

        assertThat(ex.getMessage()).contains("unknown PRNG algorithm");
    }

}
