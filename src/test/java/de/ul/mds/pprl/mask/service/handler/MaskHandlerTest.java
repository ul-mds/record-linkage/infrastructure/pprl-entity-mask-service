package de.ul.mds.pprl.mask.service.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import de.ul.mds.pprl.mask.service.*;
import de.ul.mds.pprl.mask.service.model.*;
import io.javalin.Javalin;
import org.junit.jupiter.api.*;

import java.util.*;
import java.util.stream.Collectors;

import static de.ul.mds.pprl.mask.service.handler.MaskHandler.*;
import static de.ul.mds.pprl.mask.service.model.MaskingConfiguration.builder;
import static io.javalin.testtools.JavalinTest.test;
import static java.util.stream.Collectors.toCollection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings({"resource", "ConstantConditions"})
public class MaskHandlerTest {

    static final ObjectMapper MAPPER = new ObjectMapper();

    static List<String> collectRequestBodyErrorsFrom(String s) {
        try {
            JsonNode node = MAPPER.readValue(s, JsonNode.class);
            var errorMessages = new ArrayList<String>();

            for (JsonNode errorNode : node.get("REQUEST_BODY")) {
                errorMessages.add(errorNode.get("message").asText());
            }

            return errorMessages;
        } catch (JsonProcessingException e) {
            fail("response body is not valid JSON");
        }

        return List.of();
    }

    static final List<AttributeValueEntity> DUMMY_ENTITY_LIST = List.of(new AttributeValueEntity("001", Map.of(
            "firstName", "maximilian",
            "lastName", "jugl",
            "gender", "m",
            "birthDate", "19980629"
    )));

    static List<AttributeValueEntity> mutableDummyEntityListCopy() {
        return DUMMY_ENTITY_LIST.stream()
                .map(ent -> new AttributeValueEntity(ent.getId(), new HashMap<>(ent.getAttributeValuePairs())))
                .collect(Collectors.toList());
    }

    static final List<AttributeConfiguration> DUMMY_ATTR_CONF_LIST = List.of(
            new AttributeConfiguration("firstName", Map.of("weight", 2d, "averageTokenCount", 11d)),
            new AttributeConfiguration("lastName", Map.of("weight", 2d, "averageTokenCount", 5d)),
            new AttributeConfiguration("gender", Map.of("weight", 1d, "averageTokenCount", 2d)),
            new AttributeConfiguration("birthDate", Map.of("weight", 1d, "averageTokenCount", 8d))
    );

    static List<AttributeConfiguration> mutableDummyAttrConfListCopy() {
        return DUMMY_ATTR_CONF_LIST.stream()
                .map(conf -> new AttributeConfiguration(conf.getName(), new HashMap<>(conf.getParameters())))
                .collect(Collectors.toList());
    }

    static Javalin app() {
        return new MaskApplication(new MaskApplicationProperties("Random")).app();
    }

    @Test
    public void testTryCastToLong() {
        assertThrows(IllegalArgumentException.class, () -> tryCastToLong("123"));

        int i = 123;
        Assertions.assertEquals(123L, tryCastToLong(i));
    }

    @Test
    public void testTryCastToDouble() {
        assertThrows(IllegalArgumentException.class, () -> tryCastToDouble("1.23"));

        float f = 1.23f;
        assertEquals(1.23d, tryCastToDouble(f));

        int i = 1;
        assertEquals(1d, tryCastToDouble(i), .0001d);
    }

    @Test
    public void testBadRequestOnNoHashFnDecl() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("") // hashFn cannot be empty
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("masking.hashFunction is empty");
        });
    }

    @Test
    public void testBadRequestOnTokenSizeTooSmall() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(0) // tokenSize must be >= 1
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA-256")
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("masking.tokenSize must be greater than or equal to one");
        });
    }

    @Test
    public void testBadRequestOnUnknownHashStrategy() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("foobar") // hashStrategy must be one of doubleHash, enhancedDoubleHash, randomHash
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA-256")
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("masking.hashStrategy is unknown, must be one of \"doubleHash\", \"enhancedDoubleHash\", \"randomHash\"");
        });
    }

    @Test
    public void testBadRequestOnUnknownFilterType() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("foobar") // must be one of CLK, RBF, CLKRBF
                    .paddingCharacter("_")
                    .hashFunction("SHA-256")
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("masking.filterType is unknown, must be one of \"CLK\", \"CLKRBF\", \"RBF\"");
        });
    }

    @Test
    public void testBadRequestOnPaddingTooLong() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("__") // mustn't be longer than 1 character
                    .hashFunction("SHA-256")
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("masking.padding must be at most one character");
        });
    }

    @Test
    public void testBadRequestOnHMACButNoHashFns() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("HMAC") // HMAC cannot stand alone
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("HMAC requested, but no hash functions specified");
        });
    }

    @Test
    public void testBadRequestOnHMACButNoKey() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("HMAC-SHA1") // HMAC requires a key
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("HMAC requested, but no HMAC key specified");
        });
    }

    @Test
    public void testBadRequestOnIllegalHashFn() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1-FOOBAR") // no more than two hash fns
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("illegal or unimplemented hash function \"FOOBAR\"");
        });
    }

    @Test
    public void testBadRequestOnUnimplementedHardener() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .hardener("foobar")
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("unknown hardener \"foobar\"");
        });
    }

    @Test
    public void testBadRequestOnPermuteSeedNotANumber() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .hardener("permute", Map.of("seed", "123"))
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("seed for permute hardener is not an integer");
        });
    }

    @Test
    public void testBadRequestOnRandRespSeedNotANumber() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .hardener("randomizedResponse", Map.of("seed", "123"))
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("seed for randomized response hardener is not an integer");
        });
    }

    @Test
    public void testBadRequestOnRandRespProbabilityNotAFloat() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .hardener("randomizedResponse", Map.of("seed", 123, "probability", "1.23"))
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("probability for randomized response hardener is not a floating-point number");
        });
    }

    @Test
    public void testBadRequestOnNoAttributeConfigurationsWithWeightedFilterTypes() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(), List.of()));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("RBF and CLKRBF require attribute configurations");
            }
        });
    }

    @Test
    public void testBadRequestOnAttributeConfigurationSaltNotAString() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("salt", 123)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("salt for attribute \"name\" must be a string");
            }
        });
    }

    @Test
    public void testBadRequestOnAttributeConfigurationWeightNotAFloat() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", "123")))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("attribute \"name\" must have a floating-point weight");
            }
        });
    }

    @Test
    public void testBadRequestOnAttributeConfigurationAverageTokenCountNotAFloat() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 1.23d, "averageTokenCount", "1.23")))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("attribute \"name\" must have a floating-point average token count");
            }
        });
    }

    @Test
    public void testBadRequestOnAttributeConfigurationWeightNotPositive() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 0d, "averageTokenCount", 1.23d)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("attribute \"name\" must have a positive weight");
            }
        });
    }

    @Test
    public void testBadRequestOnAttributeConfigurationAverageTokenCountNotPositive() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 1.23d, "averageTokenCount", 0d)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("attribute \"name\" must have a positive average token count");
            }
        });
    }

    @Test
    public void testBadRequestOnUnsetFilterSizeWithCLK() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .filterSize(0)
                    .hashValues(20)
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("filter size must be a positive number with CLK hashing");
        });
    }

    @Test
    public void testBadRequestOnUnsetHashValuesWithCLK() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityMaskingRequest(builder()
                    .tokenSize(2)
                    .hashStrategy("doubleHash")
                    .filterType("CLK")
                    .paddingCharacter("_")
                    .hashFunction("SHA1")
                    .filterSize(20)
                    .hashValues(0)
                    .build(), List.of()));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("hash values must be a positive number with CLK hashing");
        });
    }

    @Test
    public void testBadRequestOnSetFilterSizeWithRBFAndCLKRBF() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(20)
                        .hashValues(20)
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 1.23d, "averageTokenCount", 1.23d)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("filter size cannot be set with RBF and CLKRBF hashing");
            }
        });
    }

    @Test
    public void testBadRequestOnUnsetHashValuesWithRBFAndCLKRBF() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .hashValues(0)
                        .build(),
                        List.of(),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 1.23d, "averageTokenCount", 1.23d)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("hash values must be a positive number with RBF and CLKRBF hashing");
            }
        });
    }

    @Test
    public void testBadRequestOnSaltingKeyMissingForGlobalSalt() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("CLK", "RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterType.equals("CLK") ? 512 : 0)
                        .hashValues(5)
                        .salt("/foobar")
                        .build(),
                        DUMMY_ENTITY_LIST,
                        DUMMY_ATTR_CONF_LIST // only considered if filterType is RBF or CLKRBF
                ));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("entity with ID \"001\" is missing attribute \"foobar\" as a salting key");
            }
        });
    }

    @Test
    public void testBadRequestOnSaltingKeyMissingForAttributeSalt() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("CLK", "RBF", "CLKRBF")) {
                // this will set the attribute "foobar" as the salting key for the attribute "firstName", which should fail
                var attrList = mutableDummyAttrConfListCopy();
                attrList.get(0).getParameters().put("salt", "/foobar");

                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterType.equals("CLK") ? 512 : 0)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST,
                        attrList // only considered if filterType is RBF or CLKRBF
                ));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("entity with ID \"001\" is missing attribute \"foobar\" as a salting key");
            }
        });
    }

    @Test
    public void testBadRequestOnMissingAttributeForRBFAndCLKRBF() {
        test(app(), (server, client) -> {
            for (String filterType : Set.of("RBF", "CLKRBF")) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .hashValues(5)
                        .build(),
                        List.of(new AttributeValueEntity("001", Map.of("age", "24"))),
                        List.of(new AttributeConfiguration("name", Map.of("weight", 1d, "averageTokenCount", 10d)))));

                assertThat(resp.code()).isEqualTo(400);
                assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("entity with ID \"001\" is missing attribute \"name\"");
            }
        });
    }

    @Test
    public void testDifferentCLKOnDifferentTokenSize() {
        var clkSet = new HashSet<String>();
        var tokenSizes = Set.of(2, 3, 4);

        test(app(), (server, client) -> {
            for (var tokenSize : tokenSizes) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(tokenSize)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(tokenSizes);
    }

    @Test
    public void testDifferentCLKOnDifferentHashValues() {
        var clkSet = new HashSet<String>();
        var hashValues = Set.of(5, 10, 15);

        test(app(), (server, client) -> {
            for (var hashVal : hashValues) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(hashVal)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(hashValues);
    }

    @Test
    public void testDifferentCLKOnDifferentFilterSizes() {
        var clkSet = new HashSet<String>();
        var filterSizes = Set.of(256, 512, 1024);

        test(app(), (server, client) -> {
            for (var filterSize : filterSizes) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterSize)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(filterSizes);
    }

    @Test
    public void testDifferentCLKOnDifferentHashFunction() {
        var clkSet = new HashSet<String>();
        var hashFunctions = Set.of("MD5", "SHA1", "MD5-SHA1", "MD5-SHA256", "HMAC-MD5-SHA1", "HMAC-MD5-SHA256");

        test(app(), (server, client) -> {
            for (var hashFn : hashFunctions) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction(hashFn)
                        .key("secret") // will only be respected when the hash funciton declaration contains hmac
                        .filterSize(512)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(hashFunctions);
    }

    @Test
    public void testDifferentCLKOnDifferentFilterType() {
        var clkSet = new HashSet<String>();
        var filterTypes = Set.of("CLK", "RBF", "CLKRBF");

        test(app(), (server, client) -> {
            for (var filterType : filterTypes) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterType.equals("CLK") ? 512 : 0)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST,
                        DUMMY_ATTR_CONF_LIST // only considered if filterType is RBF or CLKRBF
                ));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(filterTypes);
    }

    @Test
    public void testDifferentCLKOnDifferentHashStrategy() {
        var clkSet = new HashSet<String>();
        var hashStrategies = Set.of("doubleHash", "enhancedDoubleHash", "randomHash");

        test(app(), (server, client) -> {
            for (var hashStrategy : hashStrategies) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy(hashStrategy)
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(hashStrategies);
    }

    @Test
    public void testDifferentCLKOnDifferentSalt() {
        var clkSet = new HashSet<String>();
        var salts = Set.of("", "salt", "pepper", "/foobar");

        test(app(), (server, client) -> {
            for (var salt : salts) {
                var entities = mutableDummyEntityListCopy();
                entities.get(0).getAttributeValuePairs().put("foobar", "foobaz");

                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(5)
                        .salt(salt)
                        .build(),
                        entities));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(salts);
    }

    @Test
    public void testDifferentCLKOnDifferentPadding() {
        var clkSet = new HashSet<String>();
        var paddings = Set.of("", ".", "*");

        test(app(), (server, client) -> {
            for (var padding : paddings) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter(padding)
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(5)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(paddings);
    }

    @Test
    public void testFilterTypeIsEntityOrderIndependent() {
        var ent1 = new AttributeValueEntity("001", Map.of("firstName", "maximilian", "lastName", "jugl"));
        var ent2 = new AttributeValueEntity("002", Map.of("firstName", "lars", "lastName", "hempel"));

        var confList = List.of(
                new AttributeConfiguration("firstName", Map.of("weight", 1, "averageTokenCount", 8)),
                new AttributeConfiguration("lastName", Map.of("weight", 1.5, "averageTokenCount", 6))
        );
        
        var filterTypes = Set.of("CLK", "RBF", "CLKRBF");

        test(app(), (server, client) -> {
            for (var filterType : filterTypes) {
                var resp1 = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterType.equals("CLK") ? 512 : 0)
                        .hashValues(5)
                        .build(),
                        List.of(ent1, ent2),
                        confList // only considered if filterType is RBF or CLKRBF
                ));

                assertThat(resp1.code()).isEqualTo(200);

                var r1 = MAPPER.readValue(resp1.body().string(), EntityMaskingResponse.class);
                assertThat(r1.getEntities()).hasSize(2);

                var clkList = r1.getEntities().stream().map(BitVectorEntity::getValue).toList();

                var resp2 = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType(filterType)
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(filterType.equals("CLK") ? 512 : 0)
                        .hashValues(5)
                        .build(),
                        List.of(ent2, ent1), // order is flipped
                        confList // only considered if filterType is RBF or CLKRBF
                ));

                assertThat(resp2.code()).isEqualTo(200);

                var r2 = MAPPER.readValue(resp2.body().string(), EntityMaskingResponse.class);
                assertThat(r2.getEntities()).hasSize(2);
                var clkListComp = r2.getEntities().stream().map(BitVectorEntity::getValue).toList();

                assertThat(clkList).containsAll(clkListComp);
            }
        });
    }

    @Test
    public void testAttributeSaltProducesDifferentCLK() {
        var filterTypes = Set.of("CLK", "RBF", "CLKRBF");

        test(app(), (server, client) -> {
            for (var filterType : filterTypes) {
                var clkSet = new HashSet<String>();
                var salts = Set.of("", "salty", "/foobar");

                for (String salt : salts) {
                    var attrConfList = new ArrayList<AttributeConfiguration>();

                    for (var attrConf : DUMMY_ATTR_CONF_LIST) {
                        // mutate the mapping to include an attribute-specific salt
                        var attrConfMap = new HashMap<>(attrConf.getParameters());
                        attrConfMap.put("salt", salt);
                        attrConfList.add(new AttributeConfiguration(attrConf.getName(), attrConfMap));
                    }

                    var entities = mutableDummyEntityListCopy();
                    entities.get(0).getAttributeValuePairs().put("foobar", "foobaz");

                    var resp = client.post("/", new EntityMaskingRequest(builder()
                            .tokenSize(2)
                            .hashStrategy("doubleHash")
                            .filterType(filterType)
                            .paddingCharacter("_")
                            .hashFunction("SHA1")
                            .filterSize(filterType.equals("CLK") ? 512 : 0)
                            .hashValues(5)
                            .build(),
                            entities,
                            attrConfList // only considered if filterType is RBF or CLKRBF
                    ));

                    assertThat(resp.code()).isEqualTo(200);

                    var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                    assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                    clkSet.add(r.getEntities().get(0).getValue());
                }

                assertThat(clkSet).hasSameSizeAs(salts); // with and without attribute salt
            }
        });
    }

    @Test
    public void testHardeners() {
        var clkSet = new HashSet<String>();
        var hardeners = List.of(
                new Hardener("permute", Map.of("seed", 727)),
                new Hardener("permute", Map.of("seed", 727727)),
                new Hardener("xorFold"),
                new Hardener("randomizedResponse", Map.of("seed", 727, "probability", .5d)),
                new Hardener("randomizedResponse", Map.of("seed", 727, "probability", .75d)),
                new Hardener("randomizedResponse", Map.of("seed", 727727, "probability", .5d)),
                new Hardener("balance")
        );

        test(app(), (server, client) -> {
            for (var hardener : hardeners) {
                var resp = client.post("/", new EntityMaskingRequest(builder()
                        .tokenSize(2)
                        .hashStrategy("doubleHash")
                        .filterType("CLK")
                        .paddingCharacter("_")
                        .hashFunction("SHA1")
                        .filterSize(512)
                        .hashValues(5)
                        .hardener(hardener)
                        .build(),
                        DUMMY_ENTITY_LIST));

                assertThat(resp.code()).isEqualTo(200);

                var r = MAPPER.readValue(resp.body().string(), EntityMaskingResponse.class);

                assertThat(r.getEntities()).hasSameSizeAs(DUMMY_ENTITY_LIST);
                clkSet.add(r.getEntities().get(0).getValue());
            }
        });

        assertThat(clkSet).hasSameSizeAs(hardeners);
    }

}
