package de.ul.mds.pprl.mask.service.model;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for attribute configurations.
 */
public class AttributeConfiguration {

    /**
     * Attribute name
     */
    private String name;

    /**
     * Attribute configuration parameters
     */
    private Map<String, Object> parameters;

    /**
     * Constructs a new attribute configuration with no name and parameters.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public AttributeConfiguration() {
        this("");
    }

    /**
     * Constructs a new attribute configuration with no parameters.
     *
     * @param name attribute name
     * @throws NullPointerException if {@code name} is {@code null}
     */
    public AttributeConfiguration(String name) {
        this(name, new HashMap<>());
    }

    /**
     * Constructs a new attribute configuration.
     *
     * @param name attribute name
     * @param parameters attribute configuration parameters
     * @throws NullPointerException if {@code name} or {@code parameters} is {@code null}
     */
    public AttributeConfiguration(String name, Map<String, Object> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    /**
     * Gets the attribute name.
     *
     * @return attribute name
     */
    public String getName() {
        return name;
    }


    /**
     * Sets the attribute name.
     *
     * @param name attribute name
     * @throws NullPointerException if the name is {@code null}
     */
    public void setName(String name) {
        this.name = requireNonNull(name);
    }

    /**
     * Gets the attribute configuration parameters.
     *
     * @return attribute configuration parameters
     */
    @JsonAnyGetter
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * Sets the attribute configuration parameters.
     *
     * @param parameters attribute configuration parameters
     * @throws NullPointerException if the mapping is {@code null}
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = requireNonNull(parameters);
    }

    /**
     * Puts a key-value parameter in the mapping for the attribute configuration.
     *
     * @param key parameter key
     * @param value parameter value
     * @throws NullPointerException if {@code key} or {@code value} is {@code null}
     */
    @JsonAnySetter
    public void putParameter(String key, Object value) {
        this.parameters.put(requireNonNull(key), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeConfiguration that = (AttributeConfiguration) o;
        return name.equals(that.name) && parameters.equals(that.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parameters);
    }

    @Override
    public String toString() {
        return "AttributeConfiguration{" +
                "name='" + name + '\'' +
                ", parameters=" + parameters +
                '}';
    }

}
