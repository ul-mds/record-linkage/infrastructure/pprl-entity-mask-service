package de.ul.mds.pprl.mask.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for entity masking requests.
 */
public class EntityMaskingRequest {

    /**
     * Masking configuration
     */
    @JsonProperty("masking")
    private MaskingConfiguration maskingConfiguration;

    /**
     * Attribute configurations
     */
    @JsonProperty("attributes")
    private List<AttributeConfiguration> attributeConfigurations;

    /**
     * Entities to mask
     */
    private List<AttributeValueEntity> entities;

    /**
     * Constructs a new entity masking request with no entities, masking configuration and attribute configurations.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public EntityMaskingRequest() {
        this(new MaskingConfiguration(), new ArrayList<>());
    }

    /**
     * Constructs a new entity masking request with no attribute configurations.
     *
     * @param maskingConfiguration masking configuration
     * @param entities entities to mask
     */
    public EntityMaskingRequest(MaskingConfiguration maskingConfiguration, List<AttributeValueEntity> entities) {
        this(maskingConfiguration, entities, new ArrayList<>());
    }

    /**
     * Constructs a new entity masking request.
     *
     * @param maskingConfiguration masking configuration
     * @param entities entities to mask
     * @param attributeConfigurations attribute configurations
     */
    public EntityMaskingRequest(MaskingConfiguration maskingConfiguration, List<AttributeValueEntity> entities, List<AttributeConfiguration> attributeConfigurations) {
        setMaskingConfiguration(maskingConfiguration);
        setAttributeConfigurations(attributeConfigurations);
        setEntities(entities);
    }

    /**
     * Gets the masking configuration.
     *
     * @return masking configuration
     */
    public MaskingConfiguration getMaskingConfiguration() {
        return maskingConfiguration;
    }

    /**
     * Sets the masking configuration.
     *
     * @param maskingConfiguration masking configuration
     * @throws NullPointerException if {@code maskingConfiguration} is {@code null}
     */
    public void setMaskingConfiguration(MaskingConfiguration maskingConfiguration) {
        this.maskingConfiguration = requireNonNull(maskingConfiguration);
    }

    /**
     * Gets the attribute configurations.
     *
     * @return attribute configurations
     */
    public List<AttributeConfiguration> getAttributeConfigurations() {
        return attributeConfigurations;
    }

    /**
     * Sets the attribute configurations.
     *
     * @param attributeConfigurations attribute configurations
     * @throws NullPointerException if {@code attributeConfigurations} is {@code null}
     */
    public void setAttributeConfigurations(List<AttributeConfiguration> attributeConfigurations) {
        this.attributeConfigurations = requireNonNull(attributeConfigurations);
    }

    /**
     * Gets the entities to mask.
     *
     * @return entities to mask
     */
    public List<AttributeValueEntity> getEntities() {
        return entities;
    }

    /**
     * Sets the entities to mask.
     *
     * @param entities entities to mask
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public void setEntities(List<AttributeValueEntity> entities) {
        this.entities = requireNonNull(entities);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityMaskingRequest that = (EntityMaskingRequest) o;
        return maskingConfiguration.equals(that.maskingConfiguration) && attributeConfigurations.equals(that.attributeConfigurations) && entities.equals(that.entities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maskingConfiguration, attributeConfigurations, entities);
    }

    @Override
    public String toString() {
        return "EntityMaskingRequest{" +
                "maskingConfiguration=" + maskingConfiguration +
                ", attributeConfigurations=" + attributeConfigurations +
                ", entities=" + entities +
                '}';
    }

}
