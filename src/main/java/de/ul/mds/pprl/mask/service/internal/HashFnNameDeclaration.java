package de.ul.mds.pprl.mask.service.internal;

/**
 * Hash function with its "pure" and HMAC variant in Java's crypto providers.
 *
 * @param hashFnName name of hash function
 * @param hmacHashFnName name of HMAC hash function
 */
public record HashFnNameDeclaration(String hashFnName, String hmacHashFnName) {
}
