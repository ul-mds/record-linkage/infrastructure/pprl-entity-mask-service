package de.ul.mds.pprl.mask.service;

import de.ul.mds.pprl.mask.service.handler.MaskHandler;
import io.javalin.Javalin;
import org.slf4j.*;

import java.io.IOException;
import java.util.Properties;
import java.util.function.Function;
import java.util.random.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class MaskApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaskApplication.class);

    private final Javalin app;

    public MaskApplication(MaskApplicationProperties props) {
        RandomGeneratorFactory<RandomGenerator> rgf;

        try {
            rgf = RandomGeneratorFactory.of(props.prngAlgorithm());
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException(format("unknown PRNG algorithm, must be one of \"%s\"",
                    RandomGeneratorFactory.all()
                            .map(RandomGeneratorFactory::name)
                            .collect(Collectors.joining("\", \""))), ex);
        }

        app = Javalin.create()
                .post("/", new MaskHandler(rgf));
    }

    public Javalin app() {
        return app;
    }

    public static void main(String[] args) {
        Properties props = new Properties();

        try {
            props.load(MaskApplication.class.getResourceAsStream("/app.properties"));
        } catch (IOException e) {
            LOGGER.warn("application properties file not found, using defaults", e);
        }

        String prngName = readStringProperty(props, "prng-algorithm");

        var appProps = new MaskApplicationProperties(prngName);

        MaskApplication srv;

        try {
            srv = new MaskApplication(appProps);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("invalid configuration", ex);
            throw new RuntimeException(ex);
        }

        LOGGER.info("starting server with properties {}", appProps);

        srv.app().start(8080);
    }

    private static String readStringProperty(Properties propFile, String propName) {
        return readProperty(propFile, propName, String::valueOf); // identity function
    }

    private static <T> T readProperty(Properties propFile, String propName, Function<String, T> propFunc) {
        String propVal = System.getProperty(format("service.%s", propName), propFile.getProperty(propName));

        if (propVal == null) {
            String msg = format("property %s isn't set in application or system properties", propName);

            LOGGER.error(msg, propFile);
            throw new RuntimeException(msg);
        }

        try {
            return propFunc.apply(propVal);
        } catch (RuntimeException ex) {
            String msg = format("failed to parse value %s for property %s", propVal, propName);

            LOGGER.error(msg, ex);
            throw new RuntimeException(msg, ex);
        }
    }

}
