package de.ul.mds.pprl.mask.service.internal;

import java.util.Optional;

public record AttributeSalt(String value, boolean referential) {

    /**
     * Salt is an empty string.
     */
    public static final AttributeSalt EMPTY = new AttributeSalt("", false);

    /**
     * Resolves a salt value into an instance of {@link AttributeSalt}.
     * If the string is {@code null} or empty, this function returns an empty {@link Optional}.
     * If the string starts with a forward slash ({@code /}), it is trimmed and the remainder is returned as a referential salt.
     * Otherwise, the string is returned as a non-referential salt.
     *
     * @param s salt value to process
     * @return (non-)referential salt, or empty {@link Optional}
     */
    public static Optional<AttributeSalt> resolve(String s) {
        if (s == null || s.isEmpty()) {
            return Optional.empty();
        }

        if (s.startsWith("/")) {
            return Optional.of(new AttributeSalt(s.substring(1), true));
        } else {
            return Optional.of(new AttributeSalt(s, false));
        }
    }

}
