package de.ul.mds.pprl.mask.service.model;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for CLK hardeners.
 */
public class Hardener {

    /**
     * Name of the CLK hardener
     */
    private String name;

    /**
     * Parameters supplied to the CLK hardener
     */
    private Map<String, Object> parameters;

    /**
     * Constructs a new CLK hardener with no name and no parameters.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public Hardener() {
        this("");
    }

    /**
     * Constructs a new CLK hardener with no parameters.
     *
     * @param name name of the CLK hardener
     * @throws NullPointerException if {@code name} is {@code null}
     */
    public Hardener(String name) {
        this(name, new HashMap<>());
    }

    /**
     * Constructs a new CLK hardener.
     *
     * @param name name of the CLK hardener
     * @param parameters parameters supplied to the CLK hardener
     * @throws NullPointerException if {@code name} or {@code parameters} is {@code null}
     */
    public Hardener(String name, Map<String, Object> parameters) {
        setName(name);
        setParameters(parameters);
    }

    /**
     * Gets the name of the CLK hardener.
     *
     * @return name of the CLK hardener
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the CLK hardener.
     *
     * @param name name of the CLK hardener
     * @throws NullPointerException if {@code name} is {@code null}
     */
    public void setName(String name) {
        this.name = requireNonNull(name);
    }

    /**
     * Gets the parameters supplied to the CLK hardener.
     *
     * @return parameters supplied to the CLK hardener
     */
    @JsonAnyGetter
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * Sets the parameters supplied to the CLK hardener.
     *
     * @param parameters parameters supplied to the CLK hardener
     * @throws NullPointerException if {@code parameters} is {@code null}
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = requireNonNull(parameters);
    }

    /**
     * Puts a key-value parameter in the mapping for the CLK hardener.
     *
     * @param key parameter key
     * @param value parameter value
     * @throws NullPointerException if {@code key} or {@code value} is {@code null}
     */
    @JsonAnySetter
    public void putParameter(String key, Object value) {
        parameters.put(requireNonNull(key), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hardener hardener = (Hardener) o;
        return name.equals(hardener.name) && parameters.equals(hardener.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parameters);
    }

    @Override
    public String toString() {
        return "Hardener{" +
                "name='" + name + '\'' +
                ", parameters=" + parameters +
                '}';
    }

}
