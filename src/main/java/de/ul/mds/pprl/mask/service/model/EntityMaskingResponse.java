package de.ul.mds.pprl.mask.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for entity masking responses.
 */
public class EntityMaskingResponse {

    /**
     * Bit vector entities
     */
    private List<BitVectorEntity> entities;

    /**
     * Constructs a new entity masking response without bit vector entities.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public EntityMaskingResponse() {
        this(new ArrayList<>());
    }

    /**
     * Constructs a new entity masking response.
     *
     * @param entities bit vector entities
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public EntityMaskingResponse(List<BitVectorEntity> entities) {
        setEntities(entities);
    }

    /**
     * Gets the bit vector entities.
     *
     * @return bit vector entities
     */
    public List<BitVectorEntity> getEntities() {
        return entities;
    }

    /**
     * Set the bit vector entities.
     *
     * @param entities bit vector entities
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public void setEntities(List<BitVectorEntity> entities) {
        this.entities = requireNonNull(entities);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityMaskingResponse that = (EntityMaskingResponse) o;
        return entities.equals(that.entities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entities);
    }

    @Override
    public String toString() {
        return "EntityMaskingResponse{" +
                "entities=" + entities +
                '}';
    }

}
