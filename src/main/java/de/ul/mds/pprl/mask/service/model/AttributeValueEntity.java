package de.ul.mds.pprl.mask.service.model;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for attribute value entities.
 */
public class AttributeValueEntity {

    /**
     * Entity ID
     */
    private String id;

    /**
     * Entity attributes
     */
    private Map<String, String> attributeValuePairs;

    /**
     * Constructs an entity with no ID and attributes.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public AttributeValueEntity() {
        this("");
    }

    /**
     * Constructs an entity with no attributes.
     *
     * @param id entity ID
     */
    public AttributeValueEntity(String id) {
        this(id, new HashMap<>());
    }

    /**
     * Constructs an entity.
     *
     * @param id entity ID
     * @param attributeValuePairs entity attributes
     */
    public AttributeValueEntity(String id, Map<String, String> attributeValuePairs) {
        setId(id);
        setAttributeValuePairs(attributeValuePairs);
    }

    /**
     * Gets the entity ID.
     *
     * @return entity ID
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the entity ID.
     *
     * @param id entity ID
     * @throws NullPointerException if {@code id} is {@code null}
     */
    public void setId(String id) {
        this.id = requireNonNull(id);
    }

    /**
     * Gets the entity attributes.
     *
     * @return entity attributes
     */
    @JsonAnyGetter
    public Map<String, String> getAttributeValuePairs() {
        return attributeValuePairs;
    }

    /**
     * Sets the entity attributes.
     *
     * @param attributeValuePairs entity attributes
     * @throws NullPointerException if {@code attributeValuePairs} is {@code null}
     */
    public void setAttributeValuePairs(Map<String, String> attributeValuePairs) {
        this.attributeValuePairs = requireNonNull(attributeValuePairs);
    }

    /**
     * Puts a key-value attribute in the mapping for the entity.
     *
     * @param attribute parameter key
     * @param value parameter value
     * @throws NullPointerException if {@code attribute} or {@code value} is {@code null}
     */
    @JsonAnySetter
    public void putAttributeValuePair(String attribute, String value) {
        this.attributeValuePairs.put(requireNonNull(attribute), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeValueEntity that = (AttributeValueEntity) o;
        return id.equals(that.id) && attributeValuePairs.equals(that.attributeValuePairs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, attributeValuePairs);
    }

    @Override
    public String toString() {
        return "AttributeValueEntity{" +
                "id='" + id + '\'' +
                ", attributeValuePairs=" + attributeValuePairs +
                '}';
    }

}
