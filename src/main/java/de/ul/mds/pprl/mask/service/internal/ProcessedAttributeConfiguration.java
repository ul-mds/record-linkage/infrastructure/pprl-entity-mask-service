package de.ul.mds.pprl.mask.service.internal;

import java.util.Optional;

public record ProcessedAttributeConfiguration(Optional<AttributeSalt> salt, double weight, double averageTokenCount) {
}
