package de.ul.mds.pprl.mask.service.handler;

import de.ul.mds.pprl.bloom.CLK;
import de.ul.mds.pprl.bloom.harden.*;
import de.ul.mds.pprl.mask.service.internal.*;
import de.ul.mds.pprl.mask.service.model.*;
import io.javalin.http.*;
import io.javalin.validation.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.*;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.random.*;

import static de.ul.mds.pprl.bloom.CLKUtils.*;
import static de.ul.mds.pprl.bloom.StringTokenizer.tokenize;
import static de.ul.mds.pprl.mask.service.handler.MaskHandler.Constants.*;
import static java.lang.String.format;
import static java.util.Map.entry;
import static java.util.Objects.requireNonNull;

public class MaskHandler implements Handler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaskHandler.class);

    final RandomGeneratorFactory<RandomGenerator> rngFactory;

    interface Constants {

        String FILTER_TYPE_CLK = "CLK";
        String FILTER_TYPE_RBF = "RBF";
        String FILTER_TYPE_CLKRBF = "CLKRBF";

        Set<String> FILTER_TYPES = Set.of(FILTER_TYPE_CLK, FILTER_TYPE_RBF, FILTER_TYPE_CLKRBF);

        String ATTR_PARAM_SALT = "salt";
        String ATTR_PARAM_WEIGHT = "weight";
        String ATTR_PARAM_AVERAGE_TOKEN_COUNT = "averageTokenCount";

        String HASH_STRATEGY_DOUBLE_HASH = "doubleHash";
        String HASH_STRATEGY_ENHANCED_DOUBLE_HASH = "enhancedDoubleHash";
        String HASH_STRATEGY_RANDOM_HASH = "randomHash";

        Set<String> HASH_STRATEGIES = Set.of(HASH_STRATEGY_DOUBLE_HASH, HASH_STRATEGY_ENHANCED_DOUBLE_HASH, HASH_STRATEGY_RANDOM_HASH);

    }

    /**
     * Map of hash function names to internal JCA names.
     */
    final Map<String, HashFnNameDeclaration> hashFnLookupMap = Map.ofEntries(
            entry("MD5", new HashFnNameDeclaration("MD5", "HmacMD5")),
            entry("SHA1", new HashFnNameDeclaration("SHA-1", "HmacSHA1")),
            entry("SHA256", new HashFnNameDeclaration("SHA-256", "HmacSHA256")),
            entry("SHA512", new HashFnNameDeclaration("SHA-512", "HmacSHA512"))
    );

    private static <T> ValidationException newBodyValidationException(String message, T value) {
        return new ValidationException(Map.of("REQUEST_BODY", List.of(
                new ValidationError<>(message, Map.of(), value)
        )));
    }

    public MaskHandler(RandomGeneratorFactory<RandomGenerator> rgf) {
        this.rngFactory = requireNonNull(rgf);
    }

    @Override
    public void handle(@NotNull Context ctx) {
        // trivial validation
        var req = ctx.bodyValidator(EntityMaskingRequest.class)
                .check(r -> r.getMaskingConfiguration().getTokenSize() >= 1, "masking.tokenSize must be greater than or equal to one")
                .check(r -> HASH_STRATEGIES.contains(r.getMaskingConfiguration().getHashStrategy()),
                        format("masking.hashStrategy is unknown, must be one of \"%s\"",
                                String.join("\", \"", HASH_STRATEGIES.stream().sorted().toList())))
                .check(r -> FILTER_TYPES.contains(r.getMaskingConfiguration().getFilterType()),
                        format("masking.filterType is unknown, must be one of \"%s\"",
                                String.join("\", \"", FILTER_TYPES.stream().sorted().toList())))
                .check(r -> r.getMaskingConfiguration().getPaddingCharacter().length() <= 1, "masking.padding must be at most one character")
                .check(r -> !r.getMaskingConfiguration().getHashFunction().isEmpty(), "masking.hashFunction is empty")
                .get();

        // resolve hash function declaration (e.g. HMAC-MD5-SHA256) into actual hash functions
        var hashFns = resolveHashFunctions(req);
        // resolve hardeners into functions
        var hardeners = resolveHardeners(req);
        // resolve attribute configurations
        var attrMap = resolveAttributeConfiguration(req);
        // filter type is already validated
        String filterType = req.getMaskingConfiguration().getFilterType();
        // fetch filter size and hash values. no special validation required (yet, since it's handled later) ...
        int filterSize = req.getMaskingConfiguration().getFilterSize();
        int hashValues = req.getMaskingConfiguration().getHashValues();

        // ... and by that i mean right now!
        if (filterType.equals(FILTER_TYPE_CLK)) {
            // CLK requires manual specification of filter size and hash values, so these cannot be unset
            if (filterSize <= 0) throw newBodyValidationException("filter size must be a positive number with CLK hashing", req);
            if (hashValues <= 0) throw newBodyValidationException("hash values must be a positive number with CLK hashing", req);
        } else { // RBF and CLKRBF
            // RBF and CLKRBF require a hash value count, but the filter size is automatically computed, so it cannot be set
            if (filterSize != 0) throw newBodyValidationException("filter size cannot be set with RBF and CLKRBF hashing", req);
            if (hashValues <= 0) throw newBodyValidationException("hash values must be a positive number with RBF and CLKRBF hashing", req);
        }

        // resolve padding (needs an extra resolve past validation bc padding field can be empty in request)
        String padding = resolvePadding(req);
        // hash strategy is already validated
        String hashStrategy = req.getMaskingConfiguration().getHashStrategy();
        // token size is already validated
        int tokenSize = req.getMaskingConfiguration().getTokenSize();
        // global salt doesn't need validation (can be null!)
        var globalSalt = AttributeSalt.resolve(req.getMaskingConfiguration().getSalt())
                .orElse(AttributeSalt.EMPTY);

        // prepare list of encoded entities
        var entities = new ArrayList<BitVectorEntity>(req.getEntities().size());

        // now the fun stuff begins!
        switch (filterType) {
            /*
             * CLK is the "classic" masking technique. a bloom filter is instantiated with a preset filter size
             * and hash value count. all attribute tokens are hashed into the same clk.
             */
            case FILTER_TYPE_CLK -> {
                for (var entity : req.getEntities()) {
                    CLK clk = new CLK(filterSize);

                    for (var entry : entity.getAttributeValuePairs().entrySet()) {
                        String k = entry.getKey(), v = entry.getValue();
                        var attrConf = attrMap.get(k);

                        AttributeSalt salt = globalSalt;

                        // extra null check bc attribute confs are not mandatory with clks.
                        // use attribute salt, or use global salt instead
                        if (attrConf != null && attrConf.salt().isPresent()) {
                            salt = attrConf.salt().get();
                        }

                        for (String token : tokenize(v, tokenSize, padding)) {
                            String s = resolveSaltFromEntityIfNeeded(req, salt, entity);
                            populateCLK(clk, s + token, hashFns, hashStrategy, hashValues);
                        }
                    }

                    clk = chainHardeners(clk, hardeners);
                    entities.add(new BitVectorEntity(entity.getId(), hardenAndEncodeCLK(clk, hardeners)));
                }
            } // CLK
            /*
             * CLKRBF is the spiritual successor to the (slightly more complicated) RBF masking technique.
             * the ideal clk size is computed from all attributes' expected tokens. then, the amount of hash values
             * is adjusted for every attribute depending on their assigned weights. finally, tokens are inserted into
             * the clk according to their computed hash value counts.
             */
            case FILTER_TYPE_CLKRBF ->  {
                double minWeight = Double.MAX_VALUE;

                // determine the attribute with the lowest weight
                for (var attr : attrMap.values()) {
                    minWeight = Math.min(minWeight, attr.weight());
                }

                LOGGER.debug("minimum weight is {}", minWeight);

                // create mapping of attribute name -> hash value count
                var clkrbfHashValueMap = new HashMap<String, Integer>(attrMap.size());
                double totalInsertions = 0d;

                for (var attrEntry : attrMap.entrySet()) {
                    // hash value count = base hash values * attribute weight / minimum weight
                    int attrHashValues = Double.valueOf(Math.ceil(hashValues * attrEntry.getValue().weight() / minWeight)).intValue();
                    clkrbfHashValueMap.put(attrEntry.getKey(), attrHashValues);
                    totalInsertions += attrHashValues * attrEntry.getValue().averageTokenCount();

                    LOGGER.debug("attribute \"{}\" is assigned {} hash values", attrEntry.getKey(), attrHashValues);
                }

                // compute the ideal clkrbf size according to the amount of total insertions
                int clkrbfSize = optimalCLKSize(.5d, totalInsertions);
                LOGGER.debug("expecting {} total insertions, optimal CLKRBF size is {}", format("%.2f", totalInsertions), clkrbfSize);

                for (var entity : req.getEntities()) {
                    CLK clk = new CLK(clkrbfSize);

                    for (var attrEntry : attrMap.entrySet()) {
                        String attrName = attrEntry.getKey();
                        String value = entity.getAttributeValuePairs().get(attrName);

                        // throw if an attribute that is defined in the config is not present on the entity
                        if (value == null) throw newBodyValidationException(format("entity with ID \"%s\" is missing attribute \"%s\"", entity.getId(), attrName), req);

                        // use attribute salt, or use global salt instead
                        AttributeSalt salt = attrEntry.getValue().salt().orElse(globalSalt);

                        for (String token : tokenize(value, tokenSize, padding)) {
                            String s = resolveSaltFromEntityIfNeeded(req, salt, entity);

                            // use the amount of hash values that have been previously computed for this attribute
                            populateCLK(clk, s + token, hashFns, hashStrategy, clkrbfHashValueMap.get(attrName));
                        }
                    }

                    entities.add(new BitVectorEntity(entity.getId(), hardenAndEncodeCLK(clk, hardeners)));
                }
            } // CLKRBF
            /*
             * RBF is by far the most complicated masking technique. first, a filter size is computed for every attribute.
             * this is done by computing the optimal filter size for every attribute given its expected token count
             * amount of hash values. then, the size of the rbf is computed. this is done by scaling every fbf up
             * to match the attribute weight in relation to the overall weight. the largest filter size then becomes
             * the rbf filter size. finally, an attribute value is hashed into its corresponding fbf. all fbfs are then
             * inserted into the rbf by randomly sampling bits relative to the amount of "space" an attribute occupies
             * given its weight.
             */
            case FILTER_TYPE_RBF -> {
                // attr names in order
                List<String> attrNames = attrMap.keySet().stream().sorted().toList();
                // attrName -> fbf filter size
                var fbfFilterSizeMap = new HashMap<String, Integer>(attrMap.size());
                double totalWeight = 0d;

                for (String attrName : attrNames) {
                    // compute ideal fbf size
                    var attrConf = attrMap.get(attrName);
                    int attrFilterSize = optimalCLKSize(.5, attrConf.averageTokenCount() * hashValues);
                    fbfFilterSizeMap.put(attrName, attrFilterSize);
                    // keep track of total weight
                    totalWeight += attrConf.weight();

                    LOGGER.debug("attribute \"{}\" is assigned {} bits", attrName, attrFilterSize);
                }

                int rbfFilterSize = 0;

                for (String attrName : attrNames) {
                    // compute the fbf's theoretical rbf size given its weight
                    double attrWeight = attrMap.get(attrName).weight();
                    int attrFilterSize = fbfFilterSizeMap.get(attrName);
                    // rbf size = fbf size * total weight / attr weight
                    int attrRBFFilterSize = Double.valueOf(Math.ceil(attrFilterSize * totalWeight / attrWeight)).intValue();
                    LOGGER.debug("attribute \"{}\" scales to {} bits RBF filter size", attrName, attrRBFFilterSize);
                    // update rbf filter size if the computed rbf size is larger than the current highest
                    rbfFilterSize = Math.max(rbfFilterSize, attrRBFFilterSize);
                }

                LOGGER.debug("RBF filter size is {} bits", rbfFilterSize);

                for (var entity : req.getEntities()) {
                    // construct a list of fbfs
                    var attrCLKs = new ArrayList<CLK>(attrMap.size());

                    for (String attrName : attrNames) {
                        String value = entity.getAttributeValuePairs().get(attrName);

                        // make sure that the attribute has a value assigned to it, if not then throw
                        if (value == null) throw newBodyValidationException(format("entity with ID \"%s\" is missing attribute \"%s\"", entity.getId(), attrName), req);

                        CLK clk = new CLK(fbfFilterSizeMap.get(attrName));
                        // use attribute salt, or use global salt instead
                        AttributeSalt salt = attrMap.get(attrName).salt().orElse(globalSalt);

                        for (String token : tokenize(value, tokenSize, padding)) {
                            String s = resolveSaltFromEntityIfNeeded(req, salt, entity);

                            // hash value into fbf first
                            populateCLK(clk, s + token, hashFns, hashStrategy, hashValues);
                        }

                        attrCLKs.add(clk);
                    }

                    // now construct the actual rbf
                    CLK clk = new CLK(rbfFilterSize);
                    // offset to keep track of the rbf region that we're currently setting
                    int offset = 0;

                    for (int i = 0; i < attrNames.size(); i++) {
                        String attrName = attrNames.get(i);
                        // get the fbf
                        CLK attrCLK = attrCLKs.get(i);
                        double weight = attrMap.get(attrName).weight();

                        var rng = rngFactory.create(req.getMaskingConfiguration().getSeed());

                        // compute the relative weight that this fbf has and ...
                        double relWeight = weight / totalWeight;
                        // ... compute the amount of bits that it therefore claims in the rbf.
                        // this is computed with relWeight * rbfFilterSize
                        int fbfSizeInRbf = Double.valueOf(Math.floor(relWeight * rbfFilterSize)).intValue();

                        for (int j = 0; j < fbfSizeInRbf; j++) {
                            // sample a random bit from the fbf
                            int b = rng.nextInt(attrCLK.getSize());

                            // check if the bit is set
                            if (attrCLK.getBitSet().get(b)) {
                                // if so, set that bit in the rbf
                                setBit(clk, offset + j);
                            }
                        }

                        // update the offset
                        offset += fbfSizeInRbf;
                    }

                    entities.add(new BitVectorEntity(entity.getId(), hardenAndEncodeCLK(clk, hardeners)));
                }
            } // RBF
        }

        ctx.json(new EntityMaskingResponse(entities));
    }

    /**
     * Applies a series of hardeners on a CLK and encodes the bit set behind the CLK into URL-safe Base64.
     *
     * @param clk CLK to harden and encode
     * @param hardeners list of hardeners to apply
     * @return CLK after hardening in URL-safe Base64
     */
    String hardenAndEncodeCLK(CLK clk, List<CLKHardener> hardeners) {
        return Base64.getUrlEncoder().encodeToString(chainHardeners(clk, hardeners).getBitSet().toByteArray());
    }

    /**
     * Attempts to resolve the provided salt.
     * If it is referential, the salt value is interpreted as an attribute name.
     * The actual salt value is then taken from the entity itself.
     * If the entity doesn't contain an attribute referenced by the salt, this function throws an error.
     * If it is non-referential, the value contained within the salt is returned as-is.
     *
     * @param req incoming request
     * @param salt salt object
     * @param entity entity to resolve salt from, if salt is referential
     * @return resolved salt value
     */
    String resolveSaltFromEntityIfNeeded(EntityMaskingRequest req, AttributeSalt salt, AttributeValueEntity entity) {
        if (!salt.referential()) {
            return salt.value();
        }

        String attributeName = salt.value();
        String attributeValue = entity.getAttributeValuePairs().get(attributeName);

        if (attributeValue == null) {
            throw newBodyValidationException(format("entity with ID \"%s\" is missing attribute \"%s\" as a salting key",
                    entity.getId(), attributeName), req);
        }

        return attributeValue;
    }

    /**
     * Populates a CLK with the given value, hash functions, hash strategy and amount of hash values.
     *
     * @param clk CLK to populate
     * @param value value to insert into CLK
     * @param hashFns hash functions to generate digests with
     * @param hashStrategy hash strategy to use when processing hash digests into hash values
     * @param hashValues amount of hash values to generate
     */
    void populateCLK(CLK clk, String value, List<Function<byte[], byte[]>> hashFns, String hashStrategy, int hashValues) {
        long h = 0L;
        byte[] valueBytes = value.getBytes(StandardCharsets.UTF_8);

        // currently random hashing is the only hash strategy that requires one single hash value
        boolean hashStrategyRequiresOneValue = hashStrategy.equals(HASH_STRATEGY_RANDOM_HASH);

        // compute the hash digests using the given hash functions
        for (var hashFn : hashFns) {
            h ^= bytesToLong(hashFn.apply(valueBytes));
        }

        // we will need at most two hash values
        int h1, h2 = 0;

        if (hashStrategyRequiresOneValue) {
            h1 = splitLongToInt(h);
        } else {
            int[] hInt = splitLongToInts(h);
            h1 = hInt[0]; h2 = hInt[1];
        }

        // now simply assign h1 and h2 to the respective hash strategies
        switch (hashStrategy) {
            case HASH_STRATEGY_DOUBLE_HASH -> doubleHash(clk, hashValues, h1, h2);
            case HASH_STRATEGY_ENHANCED_DOUBLE_HASH -> enhancedDoubleHash(clk, hashValues, h1, h2);
            case HASH_STRATEGY_RANDOM_HASH -> randomHash(clk, hashValues, h1, rngFactory);
        }
    }

    /**
     * Resolves the padding character from an incoming masking request.
     * If it is empty, the default character {@code _} is used.
     * If a character is specified, but it's not exactly one codepoint long, this function throws an error.
     *
     * @param req incoming mask request
     * @return padding character
     */
    String resolvePadding(EntityMaskingRequest req) {
        String p = req.getMaskingConfiguration().getPaddingCharacter();

        return p.isEmpty() ? "_" : p;
    }

    /**
     * Validates and parses attribute configurations on an incoming masking request.
     * For RBF and CLKRBF masking, attribute configurations <b>must</b> be present, and attribute weights and average token counts <b>must</b> be set.
     * Every attribute <b>can</b> contain a salt.
     *
     * @param req incoming request
     * @return map of attribute names to parsed attribute configurations
     */
    Map<String, ProcessedAttributeConfiguration> resolveAttributeConfiguration(EntityMaskingRequest req) {
        var attrConfigs = req.getAttributeConfigurations();

        // check if the filter type requires weighted attributes (RBF and CLKRBF). if so, then we need to
        // do some additional checks.
        String filterType = req.getMaskingConfiguration().getFilterType();
        boolean isWeightedFilterType = filterType.equals(FILTER_TYPE_RBF) || filterType.equals(FILTER_TYPE_CLKRBF);

        if (attrConfigs.isEmpty()) {
            // weights are *required* for RBF and CLKRBF, therefore no configs is an error.
            if (isWeightedFilterType) {
                throw newBodyValidationException("RBF and CLKRBF require attribute configurations", req);
            }

            // otherwise return an empty immutable map.
            return Map.of();
        }

        // prepare the mapping of attribute name -> attribute config
        var attrMap = new HashMap<String, ProcessedAttributeConfiguration>(attrConfigs.size());

        for (var conf : attrConfigs) {
            // init salt, weight and average token count with default values.
            String salt = "";
            double weight = 0;
            double averageTokenCount = 0;

            Object saltParam = conf.getParameters().get(ATTR_PARAM_SALT);

            // salt is optional, so it can be null.
            if (saltParam != null) {
                // check that the salt is actually a string and, if so, update the salt.
                if (!(saltParam instanceof String s)) {
                    throw newBodyValidationException(format("salt for attribute \"%s\" must be a string", conf.getName()), req);
                }

                salt = s;
            }

            // check if weight and average token count are set.
            if (isWeightedFilterType) {
                Object weightParam = conf.getParameters().get(ATTR_PARAM_WEIGHT);
                Object averageTokenCountParam = conf.getParameters().get(ATTR_PARAM_AVERAGE_TOKEN_COUNT);

                // check that weight is a floating point number
                try {
                    weight = tryCastToDouble(weightParam);
                } catch (IllegalArgumentException ex) {
                    throw newBodyValidationException(format("attribute \"%s\" must have a floating-point weight", conf.getName()), req);
                }

                // check that average token count is a floating point number
                try {
                    averageTokenCount = tryCastToDouble(averageTokenCountParam);
                } catch (IllegalArgumentException ex) {
                    throw newBodyValidationException(format("attribute \"%s\" must have a floating-point average token count", conf.getName()), req);
                }

                // check that weight is positive
                if (weight <= 0) {
                    throw newBodyValidationException(format("attribute \"%s\" must have a positive weight", conf.getName()), req);
                }

                // check that average token count is positive
                if (averageTokenCount <= 0) {
                    throw newBodyValidationException(format("attribute \"%s\" must have a positive average token count", conf.getName()), req);
                }
            }

            attrMap.put(conf.getName(), new ProcessedAttributeConfiguration(AttributeSalt.resolve(salt), weight, averageTokenCount));
        }

        return attrMap;
    }

    /**
     * Parses the list of hardeners in an incoming request.
     * Validates and applies parameters as required.
     *
     * @param req incoming request
     * @return hardening functions
     */
    List<CLKHardener> resolveHardeners(EntityMaskingRequest req) {
        var hardenerDefs = req.getMaskingConfiguration().getHardeners();
        var hardeners = new ArrayList<CLKHardener>(hardenerDefs.size());

        // keep global seed as fallback in case hardener has a custom seed set
        long globalSeed = req.getMaskingConfiguration().getSeed();

        for (Hardener h : hardenerDefs) {
            String hName = h.getName();

            try {
                switch (hName) {
                    case "balance" -> hardeners.add(resolveBalancerHardener());
                    case "permute" -> {
                        // set the seed if it is not present on the hardener params
                        h.getParameters().putIfAbsent("seed", globalSeed);
                        hardeners.add(resolvePermuteHardener(h.getParameters()));
                    }
                    case "randomizedResponse" -> {
                        // set the seed if it is not present on the hardener params
                        h.getParameters().putIfAbsent("seed", globalSeed);
                        hardeners.add(resolveRandomizedResponseHardener(h.getParameters()));
                    }
                    case "xorFold" -> hardeners.add(resolveXorFoldHardener());
                    default -> throw newBodyValidationException(format("unknown hardener \"%s\"", hName), req);
                }
            } catch (IllegalArgumentException ex) {
                throw newBodyValidationException(ex.getMessage(), req);
            }
        }

        return hardeners;
    }

    /**
     * Constructs a new instance of {@link BalanceHardener}.
     *
     * @return new {@link BalanceHardener} instance
     */
    CLKHardener resolveBalancerHardener() {
        return new BalanceHardener();
    }

    /**
     * Constructs a new instance of {@link PermuteHardener}.
     * The parameters must contain a {@code seed} key which has an integer value.
     *
     * @param params hardener parameters
     * @return new {@link PermuteHardener} instance
     * @throws IllegalArgumentException if the {@code seed} value is not an integer
     */
    CLKHardener resolvePermuteHardener(Map<String, Object> params) {
        Object seedParam = params.get("seed");
        long seed;

        try {
            seed = tryCastToLong(seedParam);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("seed for permute hardener is not an integer");
        }

        return new PermuteHardener(() -> rngFactory.create(seed));
    }

    /**
     * Constructs a new instance of {@link RandomizedResponseHardener}.
     * The parameters must contain a {@code seed} key with an integer value and a {@code probability} key with a floating-point value.
     *
     * @param params hardener parameters
     * @return new {@link RandomizedResponseHardener} instance
     * @throws IllegalArgumentException if {@code seed} is not an integer or {@code probability} is not a floating-point value or out-of-bounds
     */
    CLKHardener resolveRandomizedResponseHardener(Map<String, Object> params) {
        Object seedParam = params.get("seed");
        Object probParam = params.get("probability");

        long seed; double prob;

        try {
            seed = tryCastToLong(seedParam);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("seed for randomized response hardener is not an integer", ex);
        }

        try {
            prob = tryCastToDouble(probParam);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("probability for randomized response hardener is not a floating-point number", ex);
        }

        return new RandomizedResponseHardener(() -> rngFactory.create(seed), prob);
    }

    /**
     * Constructs a new {@link XORFoldHardener} instance.
     *
     * @return new {@link XORFoldHardener} instance
     */
    CLKHardener resolveXorFoldHardener() {
        return new XORFoldHardener();
    }

    /**
     * Validates and constructs hash functions around the declaration in an incoming request.
     *
     * @param req incoming request
     * @return hash function callbacks
     */
    List<Function<byte[], byte[]>> resolveHashFunctions(EntityMaskingRequest req) {
        // contains the hash function declaration
        String hashFnDecl = req.getMaskingConfiguration().getHashFunction();
        // flag whether to use HMAC or not
        boolean useHmac = false;
        // will be set if necessary
        String hmacKey = "";
        // contains all parts separated by "-"
        String[] hashFnDeclParts = hashFnDecl.split("-");

        if (hashFnDeclParts[0].equals("HMAC")) {
            // toggle flag and trim HMAC prefix from declaration
            useHmac = true;
            hashFnDeclParts = Arrays.copyOfRange(hashFnDeclParts, 1, hashFnDeclParts.length);

            // check if any parts remain in the declaration
            if (hashFnDeclParts.length == 0) {
                throw newBodyValidationException("HMAC requested, but no hash functions specified", req);
            }

            hmacKey = req.getMaskingConfiguration().getKey();

            if (hmacKey.length() == 0) {
                throw newBodyValidationException("HMAC requested, but no HMAC key specified", req);
            }
        }

        byte[] hmacKeyBytes = hmacKey.getBytes(StandardCharsets.UTF_8);
        List<Function<byte[], byte[]>> hashFns = new ArrayList<>(hashFnDeclParts.length);

        for (String hashFnDeclName : hashFnDeclParts) {
            // resolve the hash function name into the jca hash function name
            HashFnNameDeclaration d = hashFnLookupMap.get(hashFnDeclName);

            if (d == null) {
                throw newBodyValidationException(format("illegal or unimplemented hash function \"%s\"", hashFnDeclName), req);
            }

            // depending on whether hmac is required, the jca hash function name might be different (why??)
            if (!useHmac) {
                hashFns.add(b -> computeHash(d.hashFnName(), b));
            } else {
                hashFns.add(b -> computeHmac(d.hmacHashFnName(), b, hmacKeyBytes));
            }
        }

        return hashFns;
    }

    /**
     * Checks whether an object is a 32- or 64-bit integer.
     *
     * @param o object to check
     * @return {@code true} if the object can be cast to {@link Integer} or {@link Long}, {@code false} otherwise
     */
    static boolean isInteger(Object o) {
        return (o instanceof Integer) || (o instanceof Long);
    }

    /**
     * Checks whether an object is a 32- or 64-bit integer or floating-point number.
     *
     * @param o object to check
     * @return {@code true} if the object can be cast to {@link Integer}, {@link Long}, {@link Float} or {@link Double}, {@code false} otherwise
     */
    static boolean isDecimal(Object o) {
        return (o instanceof Double) || (o instanceof Float) || isInteger(o);
    }

    /**
     * Tries to cast an object to an instance of {@link Long}.
     * Both 32-bit and 64-bit integers are coerced into {@link Long}.
     *
     * @param o object to cast
     * @return object as {@link Long}
     * @throws IllegalArgumentException if the object is not a 32- or 64-bit integer
     */
    static long tryCastToLong(Object o) {
        if (!isInteger(o)) throw new IllegalArgumentException("object is not an integer");

        return Long.parseLong(o.toString(), 10);
    }

    /**
     * Tries to cast an object to an instance of {@link Double}.
     * 32-bit and 64-bit floating point numbers and integers are coerced into {@link Double}.
     *
     * @param o object to cast
     * @return object as {@link Double}
     * @throws IllegalArgumentException if the object is not a 32- or 64-bit integer or floating-point number
     */
    static double tryCastToDouble(Object o) {
        if (!isDecimal(o)) throw new IllegalArgumentException("object is not a floating-point number");

        return Double.parseDouble(o.toString());
    }

}
