package de.ul.mds.pprl.mask.service.model;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Model class for bit vector entities.
 */
public class BitVectorEntity {

    /**
     * Bit vector ID
     */
    private String id;

    /**
     * Bit vector value
     */
    private String value;

    /**
     * Constructs a new bit vector entity without an ID and a value.
     */
    @SuppressWarnings("unused") // empty constructor for jackson
    public BitVectorEntity() {
        this("", "");
    }

    /**
     * Constructs a new bit vector entity.
     *
     * @param id bit vector ID
     * @param value bit vector value
     * @throws NullPointerException if {@code id} or {@code value} are {@code null}
     */
    public BitVectorEntity(String id, String value) {
        setId(id);
        setValue(value);
    }

    /**
     * Gets the bit vector ID.
     *
     * @return bit vector ID
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the bit vector ID.
     *
     * @param id bit vector ID
     * @throws NullPointerException if {@code id} is {@code null}
     */
    public void setId(String id) {
        this.id = requireNonNull(id);
    }

    /**
     * Gets the bit vector value.
     *
     * @return bit vector value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the bit vector value.
     *
     * @param value bit vector value
     * @throws NullPointerException if {@code value} is {@code null}
     */
    public void setValue(String value) {
        this.value = requireNonNull(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BitVectorEntity that = (BitVectorEntity) o;
        return id.equals(that.id) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value);
    }

    @Override
    public String toString() {
        return "BitVectorEntity{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

}
