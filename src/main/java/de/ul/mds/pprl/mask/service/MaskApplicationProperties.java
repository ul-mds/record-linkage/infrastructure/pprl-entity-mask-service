package de.ul.mds.pprl.mask.service;

public record MaskApplicationProperties(String prngAlgorithm) {
}
