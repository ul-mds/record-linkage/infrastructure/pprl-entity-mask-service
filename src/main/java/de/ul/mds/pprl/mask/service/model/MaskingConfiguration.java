package de.ul.mds.pprl.mask.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class for masking configurations.
 */
public class MaskingConfiguration {

    /**
     * Token size
     */
    private int tokenSize;

    /**
     * Amount of hash values
     */
    private int hashValues;

    /**
     * Filter size
     */
    private int filterSize;

    /**
     * Hash function declaration
     */
    private String hashFunction;

    /**
     * HMAC key
     */
    private String key;

    /**
     * RNG seed
     */
    private long seed;

    /**
     * Global attribute salt
     */
    private String salt;

    /**
     * Hash strategy
     */
    private String hashStrategy;

    /**
     * Filter type
     */
    private String filterType;

    /**
     * Padding character
     */
    private String paddingCharacter;

    /**
     * List of CLK hardeners
     */
    private List<Hardener> hardeners;

    /**
     * Constructs an empty masking configuration.
     *
     * @see #builder()
     */
    public MaskingConfiguration() {
        this(0, 0, 0, "", "", 0, "", "", "", "", new ArrayList<>());
    }

    private MaskingConfiguration(
            int tokenSize,
            int hashValues,
            int filterSize,
            String hashFunction,
            String key,
            long seed,
            String salt,
            String hashStrategy,
            String filterType,
            String paddingCharacter,
            List<Hardener> hardeners
    ) {
        setTokenSize(tokenSize);
        setHashValues(hashValues);
        setFilterSize(filterSize);
        setHashFunction(hashFunction);
        setKey(key);
        setSeed(seed);
        setSalt(salt);
        setHashStrategy(hashStrategy);
        setFilterType(filterType);
        setPaddingCharacter(paddingCharacter);
        setHardeners(hardeners);
    }

    private MaskingConfiguration(Builder builder) {
        this(
                builder.tokenSize,
                builder.hashValues,
                builder.filterSize,
                builder.hashFunction,
                builder.key,
                builder.seed,
                builder.salt,
                builder.hashStrategy,
                builder.filterType,
                builder.paddingCharacter,
                builder.hardeners
        );
    }

    /**
     * Gets the token size.
     *
     * @return token size
     */
    public int getTokenSize() {
        return tokenSize;
    }

    /**
     * Sets the token size.
     *
     * @param tokenSize token size
     */
    public void setTokenSize(int tokenSize) {
        this.tokenSize = tokenSize;
    }

    /**
     * Gets the amount of hash values.
     *
     * @return amount of hash values
     */
    public int getHashValues() {
        return hashValues;
    }

    /**
     * Sets the amount of hash values.
     *
     * @param hashValues amount of hash values
     */
    public void setHashValues(int hashValues) {
        this.hashValues = hashValues;
    }

    /**
     * Gets the filter size.
     *
     * @return filter size
     */
    public int getFilterSize() {
        return filterSize;
    }

    /**
     * Sets the filter size.
     *
     * @param filterSize filter size
     */
    public void setFilterSize(int filterSize) {
        this.filterSize = filterSize;
    }

    /**
     * Gets the hash function declaration.
     *
     * @return hash function declaration
     */
    public String getHashFunction() {
        return hashFunction;
    }

    /**
     * Sets the hash function declaration.
     *
     * @param hashFunction hash function declaration
     * @throws NullPointerException if {@code hashFunction} is {@code null}
     */
    public void setHashFunction(String hashFunction) {
        this.hashFunction = requireNonNull(hashFunction);
    }

    /**
     * Gets the HMAC key.
     *
     * @return HMAC key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the HMAC key.
     *
     * @param key HMAC key
     * @throws NullPointerException if {@code key} is {@code null}
     */
    public void setKey(String key) {
        this.key = requireNonNull(key);
    }

    /**
     * Gets the global RNG seed.
     *
     * @return global RNG seed
     */
    public long getSeed() {
        return seed;
    }

    /**
     * Sets the global RNG seed.
     *
     * @param seed global RNG seed
     */
    public void setSeed(long seed) {
        this.seed = seed;
    }

    /**
     * Gets the global attribute salt.
     *
     * @return global attribute salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Sets the global attribute salt.
     *
     * @param salt global attribute salt
     * @throws NullPointerException if {@code salt} is {@code null}
     */
    public void setSalt(String salt) {
        this.salt = requireNonNull(salt);
    }

    /**
     * Gets the hash strategy.
     *
     * @return hash strategy
     */
    public String getHashStrategy() {
        return hashStrategy;
    }

    /**
     * Sets the hash strategy.
     *
     * @param hashStrategy hash strategy
     * @throws NullPointerException if {@code hashStrategy} is {@code null}
     */
    public void setHashStrategy(String hashStrategy) {
        this.hashStrategy = requireNonNull(hashStrategy);
    }

    /**
     * Gets the filter type.
     *
     * @return filter type
     */
    public String getFilterType() {
        return filterType;
    }

    /**
     * Sets the filter type.
     *
     * @param filterType filter type
     * @throws NullPointerException if {@code filterType} is {@code null}
     */
    public void setFilterType(String filterType) {
        this.filterType = requireNonNull(filterType);
    }

    /**
     * Gets the padding character.
     *
     * @return padding character
     */
    public String getPaddingCharacter() {
        return paddingCharacter;
    }

    /**
     * Sets the padding character.
     *
     * @param paddingCharacter padding character
     * @throws NullPointerException if {@code paddingCharacter} is {@code null}
     */
    public void setPaddingCharacter(String paddingCharacter) {
        this.paddingCharacter = requireNonNull(paddingCharacter);
    }

    /**
     * Gets the list of hardeners.
     *
     * @return list of hardeners
     */
    public List<Hardener> getHardeners() {
        return hardeners;
    }

    /**
     * Sets the list of hardeners.
     *
     * @param hardeners list of hardeners
     * @throws NullPointerException if {@code hardeners} is {@code null}
     */
    public void setHardeners(List<Hardener> hardeners) {
        this.hardeners = requireNonNull(hardeners);
    }

    /**
     * Constructs a new builder with which instances of {@link MaskingConfiguration} can be created.
     *
     * @return {@link MaskingConfiguration} builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder class for {@link MaskingConfiguration}.
     */
    public static class Builder {

        int tokenSize = 0;
        int hashValues = 0;
        int filterSize = 0;
        String hashFunction = "";
        String key = "";
        long seed = 0;
        String salt = "";
        String hashStrategy = "";
        String filterType = "";
        String paddingCharacter = "";
        List<Hardener> hardeners = new ArrayList<>();

        private Builder() {}

        /**
         * Sets the token size.
         *
         * @param tokenSize token size
         * @return updated builder instance
         */
        public Builder tokenSize(int tokenSize) {
            this.tokenSize = tokenSize;
            return this;
        }

        /**
         * Sets the amount of hash values.
         *
         * @param hashValues hash values
         * @return updated builder instance
         */
        public Builder hashValues(int hashValues) {
            this.hashValues = hashValues;
            return this;
        }

        /**
         * Sets the filter size.
         *
         * @param filterSize filter size
         * @return updated builder instance
         */
        public Builder filterSize(int filterSize) {
            this.filterSize = filterSize;
            return this;
        }

        /**
         * Sets the hash function declaration.
         *
         * @param hashFunction hash function declaration
         * @return updated builder instance
         */
        public Builder hashFunction(String hashFunction) {
            this.hashFunction = hashFunction;
            return this;
        }

        /**
         * Sets the HMAC key.
         *
         * @param key HMAC key
         * @return updated builder instance
         */
        public Builder key(String key) {
            this.key = key;
            return this;
        }

        /**
         * Sets the global RNG seed.
         *
         * @param seed global RNG seed
         * @return updated builder instance
         */
        public Builder seed(long seed) {
            this.seed = seed;
            return this;
        }

        /**
         * Sets the global attribute salt.
         *
         * @param salt global attribute salt
         * @return updated builder instance
         */
        public Builder salt(String salt) {
            this.salt = salt;
            return this;
        }

        /**
         * Sets the hash strategy
         *
         * @param hashStrategy hash strategy
         * @return updated builder instance
         */
        public Builder hashStrategy(String hashStrategy) {
            this.hashStrategy = hashStrategy;
            return this;
        }

        /**
         * Sets the filter type.
         *
         * @param filterType filter type
         * @return updated builder instance
         */
        public Builder filterType(String filterType) {
            this.filterType = filterType;
            return this;
        }

        /**
         * Sets the padding character.
         *
         * @param paddingCharacter padding character
         * @return updated builder instance
         */
        public Builder paddingCharacter(String paddingCharacter) {
            this.paddingCharacter = paddingCharacter;
            return this;
        }

        /**
         * Adds a hardener.
         *
         * @param hardener hardener to add
         * @return updated builder instance
         */
        public Builder hardener(Hardener hardener) {
            this.hardeners.add(hardener);
            return this;
        }

        public Builder hardener(String hardenerName) {
            return hardener(hardenerName, Map.of());
        }

        public Builder hardener(String hardenerName, Map<String, Object> hardenerArgs) {
            this.hardeners.add(new Hardener(hardenerName, hardenerArgs));
            return this;
        }

        /**
         * Creates a new {@link MaskingConfiguration} with the arguments passed into this builder.
         *
         * @return {@link MaskingConfiguration} instance
         * @throws NullPointerException if any strings have been set to {@code null}
         */
        public MaskingConfiguration build() {
            return new MaskingConfiguration(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaskingConfiguration that = (MaskingConfiguration) o;
        return tokenSize == that.tokenSize && hashValues == that.hashValues && filterSize == that.filterSize && seed == that.seed && hashFunction.equals(that.hashFunction) && key.equals(that.key) && salt.equals(that.salt) && hashStrategy.equals(that.hashStrategy) && filterType.equals(that.filterType) && paddingCharacter.equals(that.paddingCharacter) && hardeners.equals(that.hardeners);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenSize, hashValues, filterSize, hashFunction, key, seed, salt, hashStrategy, filterType, paddingCharacter, hardeners);
    }

    @Override
    public String toString() {
        return "MaskingConfiguration{" +
                "tokenSize=" + tokenSize +
                ", hashValues=" + hashValues +
                ", filterSize=" + filterSize +
                ", hashFunction='" + hashFunction + '\'' +
                ", key='" + key + '\'' +
                ", seed=" + seed +
                ", salt='" + salt + '\'' +
                ", hashStrategy='" + hashStrategy + '\'' +
                ", filterType='" + filterType + '\'' +
                ", paddingCharacter='" + paddingCharacter + '\'' +
                ", hardeners=" + hardeners +
                '}';
    }

}
